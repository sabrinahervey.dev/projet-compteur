import { useState } from "react";
import "./Count.css"
import { click } from "@testing-library/user-event/dist/click";

function Counter () {
    const [count, setCount] = useState(0);
    //fonction pour ajouter un nombre
    function addCount() {
        setCount(count + 1)
    }//fonction pour enlever un nombre
    function decrementCount() {
        if (count > 0) {
            setCount(count - 1);
        }
    }//fonction pour remettre le compteur à 0
    function reset() {
        setCount(0);
    }

  return (
    //
    <div className="Counter" >
     <h1>Compteur : {count}</h1>
     <button class="button" onClick={addCount}>Ajoutez</button>
     <button class="button" onClick={decrementCount}>Enlever</button>
     <button class="button" onClick={reset}>reset</button>
     </div>
    
  );  
}

export default Counter;